# awesome-project-1

POC of RESTful API server written in golang

Includes JWT tokens for stateless authentication

JWT and other organizational hints from [this blog](https://medium.com/@adigunhammedolalekan/build-and-deploy-a-secure-rest-api-with-go-postgresql-jwt-and-gorm-6fadf3da505b)

## go dependencies

see mod.go file for list of dependencies (not yet validated)

## database

Uses mysql (or mariadb) driver but could use postgresql or sqlite

## configuration

.env file contains configuration settings

DO NOT add .env to git! 

.env.example shows examples of necessary keys.

## sample tests

### create user
```
curl -X POST \
  http://localhost:8000/api/user/new \
  -H 'cache-control: no-cache' \
  -d '{ "email": "user3@mail.com", "password": "secret12345" }'
  
  {
      "account": {
          "ID": 4,
          "CreatedAt": "2018-12-02T20:48:38.12954-05:00",
          "UpdatedAt": "2018-12-02T20:48:38.12954-05:00",
          "DeletedAt": null,
          "email": "user4@mail.com",
          "password": "$2a$10$fmkFLhFJSqLnv.kiGx0qaecoLT2I2DacAOC0LaaICWDGGWYUXDbCO",
          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjR9._hyPPAMxaBC2hqRoJsVQ4lQpit67Stnd7lOXEWA0p2Q"
      },
      "message": "account has been created",
      "status": true
  }
```

### log in as a user
```
curl -X POST \
  http://localhost:8000/api/user/login \
  -H 'cache-control: no-cache' \
  -d '{ "email": "user3@mail.com", "password": "secret12345" }'

  {
    "account": {
        "ID": 3,
        "CreatedAt": "2018-12-02T14:06:07-05:00",
        "UpdatedAt": "2018-12-02T14:06:07-05:00",
        "DeletedAt": null,
        "email": "user3@mail.com",
        "password": "",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjN9.GgKMn3MR-3nHmYzACpQfEBffrbLrKc5CSwlft_T7Ppc"
    },
    "message": "Logged in",
    "status": true
}
```

use JWT token from login as Bearer token in subsequent requests

### create dataset
```
curl -X POST \
  http://localhost:8000/api/dataset/ \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjN9.GgKMn3MR-3nHmYzACpQfEBffrbLrKc5CSwlft_T7Ppc' \
  -H 'cache-control: no-cache' \
  -d '{ "dataset_name": "big_giant_dataset", "dataset_id": "bgds1" }'

  {
    "dataset": {
        "ID": 1,
        "CreatedAt": "2018-12-02T20:39:42.875933-05:00",
        "UpdatedAt": "2018-12-02T20:39:42.875933-05:00",
        "DeletedAt": null,
        "dataset_name": "big_giant_dataset",
        "dataset_id": "bgds1",
        "user_id": 3
    },
    "message": "success",
    "status": true
  }
```
notice the dataset.user_id is the authenticated user's id (is owner)

### get datasets belonging to authenticated account
```
curl -X GET \
  http://localhost:8000/api/me/datasets \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjN9.GgKMn3MR-3nHmYzACpQfEBffrbLrKc5CSwlft_T7Ppc' \
  -H 'Postman-Token: 229601a2-8881-4c03-a4ab-d2035fe690a2' \
  -H 'cache-control: no-cache'
  
  {
      "data": [
          {
              "ID": 1,
              "CreatedAt": "2018-12-02T20:39:42-05:00",
              "UpdatedAt": "2018-12-02T20:39:42-05:00",
              "DeletedAt": null,
              "dataset_name": "big_giant_dataset",
              "dataset_id": "bgds1",
              "user_id": 3
          },
          {
              "ID": 2,
              "CreatedAt": "2018-12-02T20:51:12-05:00",
              "UpdatedAt": "2018-12-02T20:51:12-05:00",
              "DeletedAt": null,
              "dataset_name": "big_giant_dataset 2",
              "dataset_id": "bgds2",
              "user_id": 3
          }
      ],
      "message": "success",
      "status": true
  }
```
result is datasets where user_id=3


