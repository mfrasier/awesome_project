package app

import (
	"awesomeProject/models"
	u "awesomeProject/utils"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"net/http"
	"os"
	"strings"
)

/*
middleware to handle jwt auth
intercept every request
check for JWT token in Authorization header
verify token is authentic and valid
send error or proceed with request depending on validity
*/

type userKey string            // WithValue API doc says create an unexported type for key
const UserKey userKey = "user" // then an exported key to use

var JwtAuthentication = func(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		notAuth := []string{"/api/user/new", "/api/user/login"} // list of endpoints not requiring auth
		requestPath := r.URL.Path

		// serve request if doesn't need auth
		for _, value := range notAuth {
			if value == requestPath {
				next.ServeHTTP(w, r)
				return
			}
		}

		response := make(map[string]interface{})
		tokenHeader := r.Header.Get("Authorization")

		if tokenHeader == "" { // auth token missing, status 403
			response = u.Message(false, "Missing auth token")
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		splitted := strings.Split(tokenHeader, " ") // token normally comes in format `Bearer {token-body}`
		if len(splitted) != 2 {
			response = u.Message(false, "Invalid/Malformed auth token")
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		tokenPart := splitted[1]
		tk := &models.Token{}

		token, err := jwt.ParseWithClaims(tokenPart, tk, func(token *jwt.Token) (interface{}, error) {
			//token, err := jwt.Parse(tokenPart, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("jwt_token_password")), nil
		})

		fmt.Printf("User %s\n", tk.UserId) // useful for monitoring

		if err != nil { // malformed token
			fmt.Println(err)
			response = u.Message(false, "Malformed authentication token")
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		if !token.Valid { // token is invalid, maybe not signed on this server
			response = u.Message(false, "Token is not valid")
			w.WriteHeader(http.StatusForbidden)
			w.Header().Add("Content-Type", "application/json")
			u.Respond(w, response)
			return
		}

		context.Set(r, UserKey, tk.UserId)
		fmt.Printf("user key: %#v\n", context.Get(r, UserKey))
		next.ServeHTTP(w, r) // proceed with middleware chain
	})
}
