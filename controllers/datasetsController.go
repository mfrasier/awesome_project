package controllers

import (
	"awesomeProject/app"
	"awesomeProject/models"
	u "awesomeProject/utils"
	"encoding/json"
	"fmt"
	"github.com/gorilla/context"
	"net/http"
)

var CreateDataset = func(w http.ResponseWriter, r *http.Request) {
	userId := context.Get(r, app.UserKey) // grab id of user who sent request
	dataset := &models.Dataset{}

	err := json.NewDecoder(r.Body).Decode(dataset)
	if err != nil {
		fmt.Print(err)
		u.Respond(w, u.Message(false, "Error decoding request body"))
		return
	}

	dataset.UserId = userId.(uint)
	resp := dataset.Create()
	u.Respond(w, resp)
}

var GetDatasetsFor = func(w http.ResponseWriter, r *http.Request) {
	userId := context.Get(r, app.UserKey)
	fmt.Printf("userId: type=%T value=%d\n", userId, userId)

	data := models.GetDatasets(userId.(uint))
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}
