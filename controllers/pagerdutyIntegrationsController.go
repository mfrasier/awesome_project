package controllers

import (
	"awesomeProject/models"
	u "awesomeProject/utils"
	"encoding/json"
	"fmt"
	"net/http"
)

var CreatePagerDutyIntegration = func(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(uint) // grab id of user who sent request
	integration := models.PagerDutyIntegration{}

	err := json.NewDecoder(r.Body).Decode(integration)
	if err != nil {
		u.Respond(w, u.Message(false, "Error decoding request body"))
		return
	}

	integration.UserId = user
	resp := integration.Create()
	u.Respond(w, resp)
}

var GetPagerDutyIntegrationFor = func(w http.ResponseWriter, r *http.Request) {
	fmt.Println("not yet implemented")
	u.Respond(w, u.Message(false, "Not yet implemented"))
}
