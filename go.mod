module awesome-project-1

require (
    github.com/dgrijalva/jwt-go
    github.com/gorilla/mux
    github.com/jinzhu/gorm
    github.com/joho/godotenv
    golang.org/x/crypto
)