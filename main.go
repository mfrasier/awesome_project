package main

import (
	"awesomeProject/app"
	"awesomeProject/controllers"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"net/http"
	"os"
	"time"
)

//// UserProfile belongs to User; UserID is foreign key
//type UserProfile struct {
//	gorm.Model
//	UserID      int
//	User        User
//	FavoriteDay string
//}
//

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("in HomeHandler")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "not yet implemented")
}

func main() {
	router := mux.NewRouter()
	router.Use(app.JwtAuthentication)
	router.StrictSlash(true)

	apiSubRouter := router.PathPrefix("/api").Subrouter()

	apiUserSubRouter := apiSubRouter.PathPrefix("/user").Subrouter()
	apiUserSubRouter.HandleFunc("/new", controllers.CreateAccount).Methods("POST")
	apiUserSubRouter.HandleFunc("/login", controllers.Authenticate).Methods("POST")

	datasetSubRouter := apiSubRouter.PathPrefix("/dataset").Subrouter()
	datasetSubRouter.HandleFunc("/", controllers.CreateDataset).Methods("POST")

	// get resources for caller
	apiMeSubRouter := apiSubRouter.PathPrefix("/me").Subrouter()
	apiMeSubRouter.HandleFunc("/datasets", controllers.GetDatasetsFor).Methods("GET")

	// catch all
	router.HandleFunc("/", HomeHandler)
	//router.PathPrefix("/").Handler(HomeHandler)

	// serve static files from ./static/
	var dir string
	flag.StringVar(&dir, "dir", ".", "the directory to server files from.  defaults to the current dir")
	flag.Parse()
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))

	// start http server
	// see graceful shutdown in gorilla/mux readme
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}

	httpServer := &http.Server{
		Handler: router,
		Addr:    ":" + port,
		// enforce timeouts
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(httpServer.ListenAndServe())

	//
	//	// create profile
	//	db.Create(&UserProfile{User: user, FavoriteDay: "Wednesday"})
	//
	//	// get profile for user
	//	var profile UserProfile
	//	db.Model(&user).Related(&profile)
	//	fmt.Printf("profile: %+v\n", profile)
	//
	//	// Update
	//	db.Model(&user).Update("LastVisited", time.Now())
	//	fmt.Println("updated user.LastVisited")
	//	fmt.Printf("user: %+v\n", user)
	//
	//	// Delete
	//	db.Delete(&user)
	//	fmt.Println("soft deleted user")
}
