package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
	"os"
)

var db *gorm.DB // db instance

// connect to database on init
func init() {
	e := godotenv.Load()
	if e != nil {
		fmt.Print(e)
	}

	username := os.Getenv("db_user")
	password := os.Getenv("db_password")
	dbType := os.Getenv("db_type")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")
	dbPort := os.Getenv("db_port")

	// TODO different URI format for different database types
	// can use Config.FormatDSN() to construct DSN from struct
	// this works for mysql/mariadb over tcp
	dbUri := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		username, password, dbHost, dbPort, dbName)

	conn, err := gorm.Open(dbType, dbUri)
	if err != nil {
		fmt.Println(err)
	}

	db = conn
	db.Debug().AutoMigrate(
		&Account{},
		&Dataset{},
		&PagerDutyIntegration{})
}

// return handle to DB object
func GetDB() *gorm.DB {
	return db
}
