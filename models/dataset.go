package models

import (
	u "awesomeProject/utils"
	"fmt"
	"github.com/jinzhu/gorm"
)

type Dataset struct {
	gorm.Model
	DatasetName string `json:"dataset_name" gorm:"unique;not null"`
	DatasetId   string `json:"dataset_id" gorm:"size:32; unique;not null"`
	UserId      uint   `json:"user_id"` // the user that this dataset belongs to
}

// validate required parameters from http request
func (dataset *Dataset) Validate() (map[string]interface{}, bool) {
	if dataset.DatasetId == "" {
		return u.Message(false, "dataset_id is required in payload"), false
	}

	if dataset.DatasetName == "" {
		return u.Message(false, "dataset_name is required in payload"), false
	}

	if dataset.UserId <= 0 {
		return u.Message(false, "user_id is required in payload"), false
	}

	// required paramters are present
	return u.Message(true, "success"), true
}

// create new dataset
func (dataset *Dataset) Create() map[string]interface{} {
	if resp, ok := dataset.Validate(); !ok {
		return resp
	}

	GetDB().Create(dataset)

	resp := u.Message(true, "success")
	resp["dataset"] = dataset
	return resp
}

// get dataset by id
func GetDataset(id uint) *Dataset {
	dataset := &Dataset{}
	err := GetDB().Table("datasets").Where("id = ?", id).First(dataset).Error
	if err != nil {
		return nil
	}
	return dataset
}

// get all datasets by user
func GetDatasets(user uint) []*Dataset {
	datasets := make([]*Dataset, 0)
	err := GetDB().Table("datasets").Where("user_id = ?", user).Find(&datasets).Error

	if err != nil {
		fmt.Println(err)
		return nil
	}

	return datasets
}
