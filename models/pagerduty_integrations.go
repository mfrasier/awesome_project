package models

import (
	u "awesomeProject/utils"
	"fmt"
	"github.com/jinzhu/gorm"
)

type PagerDutyIntegration struct {
	gorm.Model
	IntegrationName string `json:"integration_name"`
	IntegrationKey  string `json:"integration_id"`
	Description     string `json:"description"`
	UserId          uint   `json:"user_id"`
}

// validate required parameters from http request
func (pagerduty_integration *PagerDutyIntegration) Validate() (map[string]interface{}, bool) {
	if pagerduty_integration.IntegrationKey == "" {
		return u.Message(false, "integration_key is required in payload"), false
	}

	if pagerduty_integration.IntegrationName == "" {
		return u.Message(false, "integration_name is required in payload"), false
	}

	if pagerduty_integration.UserId <= 0 {
		return u.Message(false, "user_id is required in payload"), false
	}

	// required paramters are present
	return u.Message(true, "success"), true
}

// create new dataset
func (pagerduty_integration *PagerDutyIntegration) Create() map[string]interface{} {
	if resp, ok := pagerduty_integration.Validate(); !ok {
		return resp
	}

	GetDB().Create(pagerduty_integration)

	resp := u.Message(true, "success")
	resp["pagerduty_integration"] = pagerduty_integration
	return resp
}

// get dataset by id
func GetPagerDutyIntegration(id uint) *PagerDutyIntegration {
	integration := &PagerDutyIntegration{}
	err := GetDB().Table("pagerduty_integrations").Where("id = ?", id).First(integration).Error
	if err != nil {
		return nil
	}
	return integration
}

// get all datasets by user
func PagerDutyIntegrations(user uint) []*PagerDutyIntegration {
	integrations := make([]*PagerDutyIntegration, 0)
	err := GetDB().Table("pagerduty_integrations").Where("user_id = ?", user).Find(&integrations).Error

	if err != nil {
		fmt.Println(err)
		return nil
	}

	return integrations
}
